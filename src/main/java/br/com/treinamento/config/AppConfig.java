package br.com.treinamento.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

// TODO: Auto-generated Javadoc
/**
 * The Class AppConfig.
 */
@Configuration
@EnableAutoConfiguration
@ComponentScan("br.com.treinamento.dojo")
public class AppConfig {
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(AppConfig.class, args);
	}
}
