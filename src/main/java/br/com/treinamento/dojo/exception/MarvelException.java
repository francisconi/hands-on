package br.com.treinamento.dojo.exception;

// TODO: Auto-generated Javadoc
/**
 * The Class MarvelException.
 */
public class MarvelException extends Exception {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 3298475561068528155L;

    /**
     * Instantiates a new marvel exception.
     *
     * @param message the message
     */
    public MarvelException(String message) {
        super(message);
    }
}
