package br.com.treinamento.dojo.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import br.com.treinamento.dojo.characters.model.Characters;
import br.com.treinamento.dojo.controller.MarvelSenderController;
import br.com.treinamento.dojo.exception.MarvelException;
import br.com.treinamento.dojo.singleton.MarvelSingleton;

// TODO: Auto-generated Javadoc
/**
 * The Class CharactersService.
 */
@Component
public class CharactersService {
	
	/** The Constant URL. */
	private static final String URL = "http://gateway.marvel.com:80/v1/public/characters";
	
	/** The marvel sender controller. */
	@Autowired
	private MarvelSenderController marvelSenderController;
	
	/** The marvel singleton. */
	@Autowired
	private MarvelSingleton marvelSingleton;
	
	/**
	 * Buscar informacoes character.
	 *
	 * @param nomeCharacter the nome character
	 * @return the string
	 * @throws MarvelException the marvel exception
	 * @throws JsonParseException the json parse exception
	 * @throws JsonMappingException the json mapping exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public String buscarInformacoesCharacter(String nomeCharacter)
            throws MarvelException, JsonParseException, JsonMappingException, IOException {
		
		if (Objects.isNull(nomeCharacter)) {
			throw new MarvelException("Erro ao carragar os dados");
		}

        String responseCharacter = buscarCharacter(nomeCharacter);
        
        ObjectMapper objectMapper = new ObjectMapper();
		Characters result = objectMapper.readValue(responseCharacter, Characters.class);
		marvelSingleton.getCharactersResults().add(result);
        
        return responseCharacter;

    }

    /**
     * Buscar character.
     *
     * @param nomeCharacter the nome character
     * @return the string
     * @throws UnsupportedEncodingException the unsupported encoding exception
     * @throws MarvelException the marvel exception
     */
    private String buscarCharacter(String nomeCharacter)
            throws UnsupportedEncodingException, MarvelException {
        return marvelSenderController.sendRequest(URL + "?name="
                + URLEncoder.encode(nomeCharacter, "UTF-8"));
    }
}



