package br.com.treinamento.dojo.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import br.com.treinamento.dojo.comics.model.Comics;
import br.com.treinamento.dojo.controller.MarvelSenderController;
import br.com.treinamento.dojo.exception.MarvelException;
import br.com.treinamento.dojo.singleton.MarvelSingleton;

// TODO: Auto-generated Javadoc
/**
 * The Class ComicsService.
 */
@Component
public class ComicsService {

	/** The Constant URL. */
	private static final String URL = "http://gateway.marvel.com:80/v1/public/comics";

	/** The marvel sender controller. */
	@Autowired
	private MarvelSenderController marvelSenderController;
	
	/** The marvel singleton. */
	@Autowired
	private MarvelSingleton marvelSingleton;

	/**
	 * Buscar comics por ID.
	 *
	 * @param idComic the id comic
	 * @return the string
	 * @throws MarvelException the marvel exception
	 * @throws JsonParseException the json parse exception
	 * @throws JsonMappingException the json mapping exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public String buscarComicsPorID(String idComic) throws MarvelException, JsonParseException, JsonMappingException, IOException {

		if (Objects.isNull(idComic)) {
			throw new MarvelException("Erro ao carragar os dados.");
		}

		int id;

		try {
			id = Integer.valueOf(idComic);
		} catch (Exception e) {
			throw new MarvelException("Erro ao carragar os dados.");
		}

		String responseCharacter = buscarComics(id);
		
		ObjectMapper objectMapper = new ObjectMapper();
		Comics result = objectMapper.readValue(responseCharacter, Comics.class);
		marvelSingleton.getComicsResults().add(result);

		return responseCharacter;
	}

	/**
	 * Buscar comics.
	 *
	 * @param characterId the character id
	 * @return the string
	 * @throws MarvelException the marvel exception
	 */
	private String buscarComics(Integer characterId) throws MarvelException {
		return marvelSenderController.sendRequest(URL + "/" + characterId);
	}

}
