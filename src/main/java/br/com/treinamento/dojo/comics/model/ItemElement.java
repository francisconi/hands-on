package br.com.treinamento.dojo.comics.model;



// TODO: Auto-generated Javadoc
/**
 * The Class ItemElement.
 */
public class ItemElement {
	
	/** The type. */
	private java.lang.String type;

	/** The resource URI. */
	private java.lang.String resourceURI;

	/** The name. */
	private java.lang.String name;

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public java.lang.String getType() {
		return type;
	}

	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(java.lang.String type) {
		this.type = type;
	}

	/**
	 * Gets the resource URI.
	 *
	 * @return the resource URI
	 */
	public java.lang.String getResourceURI() {
		return resourceURI;
	}

	/**
	 * Sets the resource URI.
	 *
	 * @param resourceURI the new resource URI
	 */
	public void setResourceURI(java.lang.String resourceURI) {
		this.resourceURI = resourceURI;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public java.lang.String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(java.lang.String name) {
		this.name = name;
	}
	
}