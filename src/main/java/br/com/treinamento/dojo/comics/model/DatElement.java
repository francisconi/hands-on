package br.com.treinamento.dojo.comics.model;


// TODO: Auto-generated Javadoc
/**
 * The Class DatElement.
 */
public class DatElement {
	
	/** The type. */
	private java.lang.String type;

	/** The date. */
	private java.lang.String date;

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public java.lang.String getType() {
		return type;
	}

	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(java.lang.String type) {
		this.type = type;
	}

	/**
	 * Gets the date.
	 *
	 * @return the date
	 */
	public java.lang.String getDate() {
		return date;
	}

	/**
	 * Sets the date.
	 *
	 * @param date the new date
	 */
	public void setDate(java.lang.String date) {
		this.date = date;
	}

}