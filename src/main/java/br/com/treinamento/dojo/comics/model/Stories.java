package br.com.treinamento.dojo.comics.model;



// TODO: Auto-generated Javadoc
/**
 * The Class Stories.
 */
public class Stories {
	
	/** The available. */
	private Integer available;

	/** The collection URI. */
	private String collectionURI;

	/** The returned. */
	private Integer returned;

	/** The items. */
	private ItemElement[] items;

	/**
	 * Gets the available.
	 *
	 * @return the available
	 */
	public Integer getAvailable() {
		return available;
	}

	/**
	 * Sets the available.
	 *
	 * @param available the new available
	 */
	public void setAvailable(Integer available) {
		this.available = available;
	}

	/**
	 * Gets the collection URI.
	 *
	 * @return the collection URI
	 */
	public String getCollectionURI() {
		return collectionURI;
	}

	/**
	 * Sets the collection URI.
	 *
	 * @param collectionURI the new collection URI
	 */
	public void setCollectionURI(String collectionURI) {
		this.collectionURI = collectionURI;
	}

	/**
	 * Gets the returned.
	 *
	 * @return the returned
	 */
	public Integer getReturned() {
		return returned;
	}

	/**
	 * Sets the returned.
	 *
	 * @param returned the new returned
	 */
	public void setReturned(Integer returned) {
		this.returned = returned;
	}

	/**
	 * Gets the items.
	 *
	 * @return the items
	 */
	public ItemElement[] getItems() {
		return items;
	}

	/**
	 * Sets the items.
	 *
	 * @param items the new items
	 */
	public void setItems(ItemElement[] items) {
		this.items = items;
	}

	
}