package br.com.treinamento.dojo.comics.model;



// TODO: Auto-generated Javadoc
/**
 * The Class Thumbnail.
 */
public class Thumbnail {
	
	/** The path. */
	private java.lang.String path;

	/** The extension. */
	private java.lang.String extension;

	/**
	 * Gets the path.
	 *
	 * @return the path
	 */
	public java.lang.String getPath() {
		return path;
	}

	/**
	 * Sets the path.
	 *
	 * @param path the new path
	 */
	public void setPath(java.lang.String path) {
		this.path = path;
	}

	/**
	 * Gets the extension.
	 *
	 * @return the extension
	 */
	public java.lang.String getExtension() {
		return extension;
	}

	/**
	 * Sets the extension.
	 *
	 * @param extension the new extension
	 */
	public void setExtension(java.lang.String extension) {
		this.extension = extension;
	}

}