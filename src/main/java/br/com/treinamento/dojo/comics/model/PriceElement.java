package br.com.treinamento.dojo.comics.model;



// TODO: Auto-generated Javadoc
/**
 * The Class PriceElement.
 */
public class PriceElement {
	
	/** The type. */
	private String type;

	/** The price. */
	private Integer price;

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Gets the price.
	 *
	 * @return the price
	 */
	public Integer getPrice() {
		return price;
	}

	/**
	 * Sets the price.
	 *
	 * @param price the new price
	 */
	public void setPrice(Integer price) {
		this.price = price;
	}

}