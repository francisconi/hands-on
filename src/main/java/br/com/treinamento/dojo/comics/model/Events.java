package br.com.treinamento.dojo.comics.model;



// TODO: Auto-generated Javadoc
/**
 * The Class Events.
 */
public class Events {
	
	/** The available. */
	private java.lang.Integer available;

	/** The collection URI. */
	private java.lang.String collectionURI	;

	/** The returned. */
	private java.lang.Integer returned;

	/** The items. */
	private java.lang.Object[] items;

	/**
	 * Gets the available.
	 *
	 * @return the available
	 */
	public java.lang.Integer getAvailable() {
		return available;
	}

	/**
	 * Sets the available.
	 *
	 * @param available the new available
	 */
	public void setAvailable(java.lang.Integer available) {
		this.available = available;
	}

	/**
	 * Gets the collection URI.
	 *
	 * @return the collection URI
	 */
	public java.lang.String getCollectionURI() {
		return collectionURI;
	}

	/**
	 * Sets the collection URI.
	 *
	 * @param collectionURI the new collection URI
	 */
	public void setCollectionURI(java.lang.String collectionURI) {
		this.collectionURI = collectionURI;
	}

	/**
	 * Gets the returned.
	 *
	 * @return the returned
	 */
	public java.lang.Integer getReturned() {
		return returned;
	}

	/**
	 * Sets the returned.
	 *
	 * @param returned the new returned
	 */
	public void setReturned(java.lang.Integer returned) {
		this.returned = returned;
	}

	/**
	 * Gets the items.
	 *
	 * @return the items
	 */
	public java.lang.Object[] getItems() {
		return items;
	}

	/**
	 * Sets the items.
	 *
	 * @param items the new items
	 */
	public void setItems(java.lang.Object[] items) {
		this.items = items;
	}

}