package br.com.treinamento.dojo.comics.model;



import com.fasterxml.jackson.annotation.JsonView;

// TODO: Auto-generated Javadoc
/**
 * The Class ResultElement.
 */
public class ResultElement {
	
	/** The prices. */
	private PriceElement[] prices;

	/** The format. */
	private String format;

	/** The dates. */
	private DatElement[] dates;

	/** The upc. */
	private String upc;

	/** The modified. */
	private String modified;

	/** The variant description. */
	private String variantDescription;

	/** The page count. */
	private Integer pageCount;

	/** The collected issues. */
	private Object[] collectedIssues;

	/** The stories. */
	private Stories stories;

	/** The creators. */
	private Creators creators;

	/** The text objects. */
	private Object[] textObjects;

	/** The urls. */
	private UrlElement[] urls;

	/** The diamond code. */
	private String diamondCode;

	/** The issue number. */
	private Integer issueNumber;

	/** The ean. */
	private String ean;

	/** The resource URI. */
	private String resourceURI;

	/** The variants. */
	private Object[] variants;

	/** The characters. */
	private Characters characters;

	/** The collections. */
	private Object[] collections;

	/** The issn. */
	private String issn;

	/** The events. */
	private Events events;

	/** The description. */
	private Object description;

	/** The digital id. */
	private Integer digitalId;

	/** The images. */
	@JsonView(Profile.PublicView.class)		
	private ImageElement[] images;

	/** The id. */
	private Integer id;

	/** The series. */
	private Series series;

	/** The title. */
	@JsonView(Profile.PublicView.class)		
	private String title;

	/** The isbn. */
	@JsonView(Profile.PublicView.class)		
	private String isbn;

	/** The thumbnail. */
	private Thumbnail thumbnail;

	/**
	 * Gets the prices.
	 *
	 * @return the prices
	 */
	public PriceElement[] getPrices() {
		return prices;
	}

	/**
	 * Sets the prices.
	 *
	 * @param prices the new prices
	 */
	public void setPrices(PriceElement[] prices) {
		this.prices = prices;
	}

	/**
	 * Gets the format.
	 *
	 * @return the format
	 */
	public String getFormat() {
		return format;
	}

	/**
	 * Sets the format.
	 *
	 * @param format the new format
	 */
	public void setFormat(String format) {
		this.format = format;
	}

	/**
	 * Gets the dates.
	 *
	 * @return the dates
	 */
	public DatElement[] getDates() {
		return dates;
	}

	/**
	 * Sets the dates.
	 *
	 * @param dates the new dates
	 */
	public void setDates(DatElement[] dates) {
		this.dates = dates;
	}

	/**
	 * Gets the upc.
	 *
	 * @return the upc
	 */
	public String getUpc() {
		return upc;
	}

	/**
	 * Sets the upc.
	 *
	 * @param upc the new upc
	 */
	public void setUpc(String upc) {
		this.upc = upc;
	}

	/**
	 * Gets the modified.
	 *
	 * @return the modified
	 */
	public String getModified() {
		return modified;
	}

	/**
	 * Sets the modified.
	 *
	 * @param modified the new modified
	 */
	public void setModified(String modified) {
		this.modified = modified;
	}

	/**
	 * Gets the variant description.
	 *
	 * @return the variant description
	 */
	public String getVariantDescription() {
		return variantDescription;
	}

	/**
	 * Sets the variant description.
	 *
	 * @param variantDescription the new variant description
	 */
	public void setVariantDescription(String variantDescription) {
		this.variantDescription = variantDescription;
	}

	/**
	 * Gets the page count.
	 *
	 * @return the page count
	 */
	public Integer getPageCount() {
		return pageCount;
	}

	/**
	 * Sets the page count.
	 *
	 * @param pageCount the new page count
	 */
	public void setPageCount(Integer pageCount) {
		this.pageCount = pageCount;
	}

	/**
	 * Gets the collected issues.
	 *
	 * @return the collected issues
	 */
	public Object[] getCollectedIssues() {
		return collectedIssues;
	}

	/**
	 * Sets the collected issues.
	 *
	 * @param collectedIssues the new collected issues
	 */
	public void setCollectedIssues(Object[] collectedIssues) {
		this.collectedIssues = collectedIssues;
	}

	/**
	 * Gets the stories.
	 *
	 * @return the stories
	 */
	public Stories getStories() {
		return stories;
	}

	/**
	 * Sets the stories.
	 *
	 * @param stories the new stories
	 */
	public void setStories(Stories stories) {
		this.stories = stories;
	}

	/**
	 * Gets the creators.
	 *
	 * @return the creators
	 */
	public Creators getCreators() {
		return creators;
	}

	/**
	 * Sets the creators.
	 *
	 * @param creators the new creators
	 */
	public void setCreators(Creators creators) {
		this.creators = creators;
	}

	/**
	 * Gets the text objects.
	 *
	 * @return the text objects
	 */
	public Object[] getTextObjects() {
		return textObjects;
	}

	/**
	 * Sets the text objects.
	 *
	 * @param textObjects the new text objects
	 */
	public void setTextObjects(Object[] textObjects) {
		this.textObjects = textObjects;
	}

	/**
	 * Gets the urls.
	 *
	 * @return the urls
	 */
	public UrlElement[] getUrls() {
		return urls;
	}

	/**
	 * Sets the urls.
	 *
	 * @param urls the new urls
	 */
	public void setUrls(UrlElement[] urls) {
		this.urls = urls;
	}

	/**
	 * Gets the diamond code.
	 *
	 * @return the diamond code
	 */
	public String getDiamondCode() {
		return diamondCode;
	}

	/**
	 * Sets the diamond code.
	 *
	 * @param diamondCode the new diamond code
	 */
	public void setDiamondCode(String diamondCode) {
		this.diamondCode = diamondCode;
	}

	/**
	 * Gets the issue number.
	 *
	 * @return the issue number
	 */
	public Integer getIssueNumber() {
		return issueNumber;
	}

	/**
	 * Sets the issue number.
	 *
	 * @param issueNumber the new issue number
	 */
	public void setIssueNumber(Integer issueNumber) {
		this.issueNumber = issueNumber;
	}

	/**
	 * Gets the ean.
	 *
	 * @return the ean
	 */
	public String getEan() {
		return ean;
	}

	/**
	 * Sets the ean.
	 *
	 * @param ean the new ean
	 */
	public void setEan(String ean) {
		this.ean = ean;
	}

	/**
	 * Gets the resource URI.
	 *
	 * @return the resource URI
	 */
	public String getResourceURI() {
		return resourceURI;
	}

	/**
	 * Sets the resource URI.
	 *
	 * @param resourceURI the new resource URI
	 */
	public void setResourceURI(String resourceURI) {
		this.resourceURI = resourceURI;
	}

	/**
	 * Gets the variants.
	 *
	 * @return the variants
	 */
	public Object[] getVariants() {
		return variants;
	}

	/**
	 * Sets the variants.
	 *
	 * @param variants the new variants
	 */
	public void setVariants(Object[] variants) {
		this.variants = variants;
	}

	/**
	 * Gets the characters.
	 *
	 * @return the characters
	 */
	public Characters getCharacters() {
		return characters;
	}

	/**
	 * Sets the characters.
	 *
	 * @param characters the new characters
	 */
	public void setCharacters(Characters characters) {
		this.characters = characters;
	}

	/**
	 * Gets the collections.
	 *
	 * @return the collections
	 */
	public Object[] getCollections() {
		return collections;
	}

	/**
	 * Sets the collections.
	 *
	 * @param collections the new collections
	 */
	public void setCollections(Object[] collections) {
		this.collections = collections;
	}

	/**
	 * Gets the issn.
	 *
	 * @return the issn
	 */
	public String getIssn() {
		return issn;
	}

	/**
	 * Sets the issn.
	 *
	 * @param issn the new issn
	 */
	public void setIssn(String issn) {
		this.issn = issn;
	}

	/**
	 * Gets the events.
	 *
	 * @return the events
	 */
	public Events getEvents() {
		return events;
	}

	/**
	 * Sets the events.
	 *
	 * @param events the new events
	 */
	public void setEvents(Events events) {
		this.events = events;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public Object getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(Object description) {
		this.description = description;
	}

	/**
	 * Gets the digital id.
	 *
	 * @return the digital id
	 */
	public Integer getDigitalId() {
		return digitalId;
	}

	/**
	 * Sets the digital id.
	 *
	 * @param digitalId the new digital id
	 */
	public void setDigitalId(Integer digitalId) {
		this.digitalId = digitalId;
	}

	/**
	 * Gets the images.
	 *
	 * @return the images
	 */
	public ImageElement[] getImages() {
		return images;
	}

	/**
	 * Sets the images.
	 *
	 * @param images the new images
	 */
	public void setImages(ImageElement[] images) {
		this.images = images;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Gets the series.
	 *
	 * @return the series
	 */
	public Series getSeries() {
		return series;
	}

	/**
	 * Sets the series.
	 *
	 * @param series the new series
	 */
	public void setSeries(Series series) {
		this.series = series;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the isbn.
	 *
	 * @return the isbn
	 */
	public String getIsbn() {
		return isbn;
	}

	/**
	 * Sets the isbn.
	 *
	 * @param isbn the new isbn
	 */
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	/**
	 * Gets the thumbnail.
	 *
	 * @return the thumbnail
	 */
	public Thumbnail getThumbnail() {
		return thumbnail;
	}

	/**
	 * Sets the thumbnail.
	 *
	 * @param thumbnail the new thumbnail
	 */
	public void setThumbnail(Thumbnail thumbnail) {
		this.thumbnail = thumbnail;
	}
	
}