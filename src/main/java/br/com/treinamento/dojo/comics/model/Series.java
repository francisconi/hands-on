package br.com.treinamento.dojo.comics.model;



// TODO: Auto-generated Javadoc
/**
 * The Class Series.
 */
public class Series {
	
	/** The resource URI. */
	private java.lang.String resourceURI;

	/** The name. */
	private java.lang.String name;

	/**
	 * Gets the resource URI.
	 *
	 * @return the resource URI
	 */
	public java.lang.String getResourceURI() {
		return resourceURI;
	}

	/**
	 * Sets the resource URI.
	 *
	 * @param resourceURI the new resource URI
	 */
	public void setResourceURI(java.lang.String resourceURI) {
		this.resourceURI = resourceURI;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public java.lang.String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(java.lang.String name) {
		this.name = name;
	}
	
}