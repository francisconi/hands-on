package br.com.treinamento.dojo.comics.model;



import com.fasterxml.jackson.annotation.JsonView;

// TODO: Auto-generated Javadoc
/**
 * The Class Data.
 */
public class Data {
	
	/** The offset. */
	private Integer offset;

	/** The limit. */
	private Integer limit;

	/** The total. */
	private Integer total;

	/** The count. */
	private Integer count;

	/** The results. */
	@JsonView(Profile.PublicView.class)		
	private ResultElement[] results;

	/**
	 * Gets the offset.
	 *
	 * @return the offset
	 */
	public Integer getOffset() {
		return offset;
	}

	/**
	 * Sets the offset.
	 *
	 * @param offset the new offset
	 */
	public void setOffset(Integer offset) {
		this.offset = offset;
	}

	/**
	 * Gets the limit.
	 *
	 * @return the limit
	 */
	public Integer getLimit() {
		return limit;
	}

	/**
	 * Sets the limit.
	 *
	 * @param limit the new limit
	 */
	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	/**
	 * Gets the total.
	 *
	 * @return the total
	 */
	public Integer getTotal() {
		return total;
	}

	/**
	 * Sets the total.
	 *
	 * @param total the new total
	 */
	public void setTotal(Integer total) {
		this.total = total;
	}

	/**
	 * Gets the count.
	 *
	 * @return the count
	 */
	public Integer getCount() {
		return count;
	}

	/**
	 * Sets the count.
	 *
	 * @param count the new count
	 */
	public void setCount(Integer count) {
		this.count = count;
	}

	/**
	 * Gets the results.
	 *
	 * @return the results
	 */
	public ResultElement[] getResults() {
		return results;
	}

	/**
	 * Sets the results.
	 *
	 * @param results the new results
	 */
	public void setResults(ResultElement[] results) {
		this.results = results;
	}
	
}