package br.com.treinamento.dojo.singleton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Singleton;

import org.springframework.stereotype.Service;

import br.com.treinamento.dojo.characters.model.Characters;
import br.com.treinamento.dojo.comics.model.Comics;


// TODO: Auto-generated Javadoc
/**
 * The Class MarvelSingleton.
 */
@Singleton
@Service
public class MarvelSingleton {    
	
	/** The comics results. */
	List<Comics> comicsResults;	
	
	/** The characters results. */
	List<Characters> charactersResults;	
	
    
    /**
     * Inits the.
     */
    @PostConstruct
    public void init() {
    	comicsResults = Collections.synchronizedList(new ArrayList<Comics>());
    	charactersResults = Collections.synchronizedList(new ArrayList<Characters>());
    }


	/**
	 * Gets the comics results.
	 *
	 * @return the comics results
	 */
	public List<Comics> getComicsResults() {
		return comicsResults;
	}


	/**
	 * Sets the comics results.
	 *
	 * @param comicsResults the new comics results
	 */
	public void setComicsResults(List<Comics> comicsResults) {
		this.comicsResults = comicsResults;
	}


	/**
	 * Gets the characters results.
	 *
	 * @return the characters results
	 */
	public List<Characters> getCharactersResults() {
		return charactersResults;
	}


	/**
	 * Sets the characters results.
	 *
	 * @param charactersResults the new characters results
	 */
	public void setCharactersResults(List<Characters> charactersResults) {
		this.charactersResults = charactersResults;
	}  
	
}
