package br.com.treinamento.dojo.characters.model;



// TODO: Auto-generated Javadoc
/**
 * The Class Thumbnail.
 */
public class Thumbnail {
	
	/** The path. */
	private String path;

	/** The extension. */
	private String extension;

	/**
	 * Gets the path.
	 *
	 * @return the path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * Sets the path.
	 *
	 * @param path the new path
	 */
	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * Gets the extension.
	 *
	 * @return the extension
	 */
	public String getExtension() {
		return extension;
	}

	/**
	 * Sets the extension.
	 *
	 * @param extension the new extension
	 */
	public void setExtension(String extension) {
		this.extension = extension;
	}

}