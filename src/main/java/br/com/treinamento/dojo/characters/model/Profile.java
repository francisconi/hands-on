package br.com.treinamento.dojo.characters.model;

// TODO: Auto-generated Javadoc
/**
 * The Class Profile.
 */
public class Profile {
	
	/**
	 * The Interface PublicView.
	 */
	public interface PublicView {}
	
	/**
	 * The Interface FriendsView.
	 */
	public interface FriendsView extends PublicView{}
	
	/**
	 * The Interface FamilyView.
	 */
	public interface FamilyView extends FriendsView {}
} 
