package br.com.treinamento.dojo.characters.model;



import com.fasterxml.jackson.annotation.JsonView;

// TODO: Auto-generated Javadoc
/**
 * The Class ResultElement.
 */
public class ResultElement {
	
	/** The urls. */
	@JsonView(Profile.PublicView.class)		
	private UrlElement[] urls;

	/** The modified. */
	private String modified;

	/** The series. */
	@JsonView(Profile.PublicView.class)		
	private Series series;

	/** The id. */
	@JsonView(Profile.PublicView.class)		
	private Integer id;

	/** The comics. */
	private Comics comics;

	/** The stories. */
	private Stories stories;

	/** The resource URI. */
	private String resourceURI;

	/** The name. */
	@JsonView(Profile.PublicView.class)		
	private String name;

	/** The events. */
	private Events events;

	/** The description. */
	private String description;

	/** The thumbnail. */
	@JsonView(Profile.PublicView.class)		
	private Thumbnail thumbnail;

	/**
	 * Gets the urls.
	 *
	 * @return the urls
	 */
	public UrlElement[] getUrls() {
		return urls;
	}

	/**
	 * Sets the urls.
	 *
	 * @param urls the new urls
	 */
	public void setUrls(UrlElement[] urls) {
		this.urls = urls;
	}

	/**
	 * Gets the modified.
	 *
	 * @return the modified
	 */
	public String getModified() {
		return modified;
	}

	/**
	 * Sets the modified.
	 *
	 * @param modified the new modified
	 */
	public void setModified(String modified) {
		this.modified = modified;
	}

	/**
	 * Gets the series.
	 *
	 * @return the series
	 */
	public Series getSeries() {
		return series;
	}

	/**
	 * Sets the series.
	 *
	 * @param series the new series
	 */
	public void setSeries(Series series) {
		this.series = series;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Gets the comics.
	 *
	 * @return the comics
	 */
	public Comics getComics() {
		return comics;
	}

	/**
	 * Sets the comics.
	 *
	 * @param comics the new comics
	 */
	public void setComics(Comics comics) {
		this.comics = comics;
	}

	/**
	 * Gets the stories.
	 *
	 * @return the stories
	 */
	public Stories getStories() {
		return stories;
	}

	/**
	 * Sets the stories.
	 *
	 * @param stories the new stories
	 */
	public void setStories(Stories stories) {
		this.stories = stories;
	}

	/**
	 * Gets the resource URI.
	 *
	 * @return the resource URI
	 */
	public String getResourceURI() {
		return resourceURI;
	}

	/**
	 * Sets the resource URI.
	 *
	 * @param resourceURI the new resource URI
	 */
	public void setResourceURI(String resourceURI) {
		this.resourceURI = resourceURI;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the events.
	 *
	 * @return the events
	 */
	public Events getEvents() {
		return events;
	}

	/**
	 * Sets the events.
	 *
	 * @param events the new events
	 */
	public void setEvents(Events events) {
		this.events = events;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the thumbnail.
	 *
	 * @return the thumbnail
	 */
	public Thumbnail getThumbnail() {
		return thumbnail;
	}

	/**
	 * Sets the thumbnail.
	 *
	 * @param thumbnail the new thumbnail
	 */
	public void setThumbnail(Thumbnail thumbnail) {
		this.thumbnail = thumbnail;
	}

}