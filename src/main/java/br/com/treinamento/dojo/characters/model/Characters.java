package br.com.treinamento.dojo.characters.model;


import com.fasterxml.jackson.annotation.JsonView;

// TODO: Auto-generated Javadoc
/**
 * The Class Characters.
 */
public class Characters {
	
	/** The code. */
	@JsonView(Profile.PublicView.class)		
    private Integer code;

	/** The copyright. */
	private String copyright;

	/** The attribution HTML. */
	private String attributionHTML;

	/** The data. */
	@JsonView(Profile.PublicView.class)		
	private Data data;

	/** The attribution text. */
	private String attributionText;

	/** The status. */
	private String status;

	/** The etag. */
	private String etag;

	/**
	 * Gets the code.
	 *
	 * @return the code
	 */
	public Integer getCode() {
		return code;
	}

	/**
	 * Sets the code.
	 *
	 * @param code the new code
	 */
	public void setCode(Integer code) {
		this.code = code;
	}

	/**
	 * Gets the copyright.
	 *
	 * @return the copyright
	 */
	public String getCopyright() {
		return copyright;
	}

	/**
	 * Sets the copyright.
	 *
	 * @param copyright the new copyright
	 */
	public void setCopyright(String copyright) {
		this.copyright = copyright;
	}

	/**
	 * Gets the attribution HTML.
	 *
	 * @return the attribution HTML
	 */
	public String getAttributionHTML() {
		return attributionHTML;
	}

	/**
	 * Sets the attribution HTML.
	 *
	 * @param attributionHTML the new attribution HTML
	 */
	public void setAttributionHTML(String attributionHTML) {
		this.attributionHTML = attributionHTML;
	}

	/**
	 * Gets the data.
	 *
	 * @return the data
	 */
	public Data getData() {
		return data;
	}

	/**
	 * Sets the data.
	 *
	 * @param data the new data
	 */
	public void setData(Data data) {
		this.data = data;
	}

	/**
	 * Gets the attribution text.
	 *
	 * @return the attribution text
	 */
	public String getAttributionText() {
		return attributionText;
	}

	/**
	 * Sets the attribution text.
	 *
	 * @param attributionText the new attribution text
	 */
	public void setAttributionText(String attributionText) {
		this.attributionText = attributionText;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Gets the etag.
	 *
	 * @return the etag
	 */
	public String getEtag() {
		return etag;
	}

	/**
	 * Sets the etag.
	 *
	 * @param etag the new etag
	 */
	public void setEtag(String etag) {
		this.etag = etag;
	}
	

}