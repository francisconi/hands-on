package br.com.treinamento.dojo.characters.model;



// TODO: Auto-generated Javadoc
/**
 * The Class ItemElement.
 */
public class ItemElement {
	
	/** The type. */
	private String type;

	/** The resource URI. */
	private String resourceURI;

	/** The name. */
	private String name;

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Gets the resource URI.
	 *
	 * @return the resource URI
	 */
	public String getResourceURI() {
		return resourceURI;
	}

	/**
	 * Sets the resource URI.
	 *
	 * @param resourceURI the new resource URI
	 */
	public void setResourceURI(String resourceURI) {
		this.resourceURI = resourceURI;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
}