package br.com.treinamento.dojo.characters.model;



// TODO: Auto-generated Javadoc
/**
 * The Class UrlElement.
 */
public class UrlElement {
	
	/** The type. */
	private String type;

	/** The url. */
	private String url;

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type.
	 *
	 * @param type the new type
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Gets the url.
	 *
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * Sets the url.
	 *
	 * @param url the new url
	 */
	public void setUrl(String url) {
		this.url = url;
	}

}