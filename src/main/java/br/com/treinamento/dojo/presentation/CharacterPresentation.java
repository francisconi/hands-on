package br.com.treinamento.dojo.presentation;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Class CharacterPresentation.
 */
public class CharacterPresentation implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 38755185087532424L;

	/** The id. */
	private Integer id;

    /** The name. */
    private String name;

    /** The description. */
    private String description;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }
}
