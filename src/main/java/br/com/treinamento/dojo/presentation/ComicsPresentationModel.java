package br.com.treinamento.dojo.presentation;

import com.fasterxml.jackson.annotation.JsonView;

import br.com.treinamento.dojo.comics.model.Comics;
import br.com.treinamento.dojo.comics.model.Profile;

// TODO: Auto-generated Javadoc
/**
 * The Class ComicsPresentationModel.
 */
public class ComicsPresentationModel {
	
	/** The comics. */
	@JsonView(Profile.PublicView.class)		
	private Comics comics;

	/**
	 * Gets the comics.
	 *
	 * @return the comics
	 */
	public Comics getComics() {
		return comics;
	}

	/**
	 * Sets the comics.
	 *
	 * @param comics the new comics
	 */
	public void setComics(Comics comics) {
		this.comics = comics;
	}	
	

}
