package br.com.treinamento.dojo.presentation;

import com.fasterxml.jackson.annotation.JsonView;

import br.com.treinamento.dojo.characters.model.Characters;
import br.com.treinamento.dojo.characters.model.Profile;

// TODO: Auto-generated Javadoc
/**
 * The Class CharactersPresentationModel.
 */
public class CharactersPresentationModel {
	
	/** The characters. */
	@JsonView(Profile.PublicView.class)		
	private Characters characters;

	/**
	 * Gets the characters.
	 *
	 * @return the characters
	 */
	public Characters getCharacters() {
		return characters;
	}

	/**
	 * Sets the characters.
	 *
	 * @param characters the new characters
	 */
	public void setCharacters(Characters characters) {
		this.characters = characters;
	}
	
	

}
