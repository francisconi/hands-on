package br.com.treinamento.dojo.presentation;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonView;

import br.com.treinamento.dojo.comics.model.Comics;
import br.com.treinamento.dojo.comics.model.Profile;

// TODO: Auto-generated Javadoc
/**
 * The Class MarvelComicsModel.
 */
public class MarvelComicsModel {
	
	/** The comics list. */
	@JsonView(Profile.PublicView.class)		
	private List<Comics> comicsList;
	
	/**
	 * Instantiates a new marvel comics model.
	 */
	public MarvelComicsModel(){
		comicsList = new ArrayList<Comics>();
	}

	/**
	 * Gets the comics list.
	 *
	 * @return the comics list
	 */
	public List<Comics> getComicsList() {
		return comicsList;
	}

	/**
	 * Sets the comics list.
	 *
	 * @param comicsList the new comics list
	 */
	public void setComicsList(List<Comics> comicsList) {
		this.comicsList = comicsList;
	}

}
