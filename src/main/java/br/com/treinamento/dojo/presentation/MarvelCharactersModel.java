package br.com.treinamento.dojo.presentation;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonView;

import br.com.treinamento.dojo.characters.model.Characters;
import br.com.treinamento.dojo.characters.model.Profile;

// TODO: Auto-generated Javadoc
/**
 * The Class MarvelCharactersModel.
 */
public class MarvelCharactersModel {
	
	/** The characters list. */
	@JsonView(Profile.PublicView.class)		
	private List<Characters> charactersList;
	
	/**
	 * Instantiates a new marvel characters model.
	 */
	public MarvelCharactersModel(){
		charactersList = new ArrayList<Characters>();
	}

	/**
	 * Gets the characters list.
	 *
	 * @return the characters list
	 */
	public List<Characters> getCharactersList() {
		return charactersList;
	}

	/**
	 * Sets the characters list.
	 *
	 * @param charactersList the new characters list
	 */
	public void setCharactersList(List<Characters> charactersList) {
		this.charactersList = charactersList;
	}
	
	

}
