package br.com.treinamento.dojo.presentation;

// TODO: Auto-generated Javadoc
/**
 * The Class ResponseComicDataRepresentation.
 */
public class ResponseComicDataRepresentation {

    /** The data. */
    private ComicPresentation data;

    /**
     * Gets the data.
     *
     * @return the data
     */
    public ComicPresentation getData() {
        return data;
    }
}
