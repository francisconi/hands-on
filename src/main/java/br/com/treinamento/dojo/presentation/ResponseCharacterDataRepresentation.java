package br.com.treinamento.dojo.presentation;

// TODO: Auto-generated Javadoc
/**
 * The Class ResponseCharacterDataRepresentation.
 */
public class ResponseCharacterDataRepresentation {

    /** The data. */
    private CharacterPresentation data;

    /**
     * Gets the data.
     *
     * @return the data
     */
    public CharacterPresentation getData() {
        return data;
    }
}
