package br.com.treinamento.dojo.presentation;

import java.io.Serializable;

// TODO: Auto-generated Javadoc
/**
 * The Class ComicPresentation.
 */
public class ComicPresentation implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 3261384102699122842L;

    /** The id. */
    private Integer id;

    /** The title. */
    private String title;

    /** The description. */
    private String description;

    /** The page count. */
    private String pageCount;

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Gets the page count.
	 *
	 * @return the page count
	 */
	public String getPageCount() {
		return pageCount;
	}

	/**
	 * Sets the page count.
	 *
	 * @param pageCount the new page count
	 */
	public void setPageCount(String pageCount) {
		this.pageCount = pageCount;
	}
    
    
}
