package br.com.treinamento.dojo.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import br.com.treinamento.dojo.comics.model.Profile;
import br.com.treinamento.dojo.presentation.MarvelComicsModel;
import br.com.treinamento.dojo.singleton.MarvelSingleton;

// TODO: Auto-generated Javadoc
/**
 * The Class MarvelComicsConsolidation.
 */
@RestController
public class MarvelComicsConsolidation {
	
	/** The marvel singleton. */
	@Autowired
	private MarvelSingleton marvelSingleton;
	
	/**
	 * Gets the comics marvel.
	 *
	 * @return the comics marvel
	 * @throws Exception the exception
	 */
	@JsonView(Profile.FriendsView.class)
	@RequestMapping(value = "/comics-marvel", method = RequestMethod.GET)
	public MarvelComicsModel getComicsMarvel() throws Exception {		
		MarvelComicsModel comicsModelList = new MarvelComicsModel();
		comicsModelList.setComicsList(marvelSingleton.getComicsResults());		
		return comicsModelList;
	}
	
	/**
	 * Delete comics.
	 *
	 * @return the response entity
	 * @throws Exception the exception
	 */
	@RequestMapping(value = "/delete-comics", method = RequestMethod.GET)
	public ResponseEntity<String> deleteComics() throws Exception {	
		marvelSingleton.getComicsResults().clear();		
		return new ResponseEntity<String>("Comics clear", HttpStatus.OK);
	}
}
