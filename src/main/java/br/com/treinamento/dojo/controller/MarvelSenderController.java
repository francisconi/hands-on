package br.com.treinamento.dojo.controller;

import java.sql.Timestamp;

import java.util.Date;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

import org.glassfish.jersey.client.ClientConfig;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;

import br.com.treinamento.dojo.exception.MarvelException;

// TODO: Auto-generated Javadoc
/**
 * The Class MarvelSenderController.
 */
@Component
public class MarvelSenderController {

	/** The Constant publicKey. */
	private static final String publicKey = "e0758cf67b4727ba7d00b140f0efddf5";

    /** The Constant privateKey. */
    private static final String privateKey = "e299edb69765cfaed8d16d89c6e06754e6b6d1cd";

    /**
     * Send request.
     *
     * @param url the url
     * @return the string
     * @throws MarvelException the marvel exception
     */
    public String sendRequest(String url) throws MarvelException {
        try {
            Timestamp ts = new Timestamp(new Date().getTime());
            String hash = ts + privateKey + publicKey;
            String md5Hash = DigestUtils.md5DigestAsHex(hash.getBytes())
                    .toString();

            ClientConfig config = new ClientConfig();
            Client client = ClientBuilder.newClient(config);

            String response = client.target(url).queryParam("ts", ts)
                    .queryParam("apikey", publicKey).queryParam("hash", md5Hash)
                    .request("application/json").get(String.class);

            return response;
        } catch (Exception e) {
            throw new MarvelException("Erro ao carragar os dados");
        }
    }
}