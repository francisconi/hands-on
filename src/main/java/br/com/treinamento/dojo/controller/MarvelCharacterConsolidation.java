package br.com.treinamento.dojo.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;

import br.com.treinamento.dojo.characters.model.Profile;
import br.com.treinamento.dojo.presentation.MarvelCharactersModel;
import br.com.treinamento.dojo.presentation.MarvelComicsModel;
import br.com.treinamento.dojo.singleton.MarvelSingleton;

// TODO: Auto-generated Javadoc
/**
 * The Class MarvelCharacterConsolidation.
 */
@RestController
public class MarvelCharacterConsolidation {
	
	/** The marvel singleton. */
	@Autowired
	private MarvelSingleton marvelSingleton;
	
	
	/**
	 * Gets the characters marvel.
	 *
	 * @return the characters marvel
	 * @throws Exception the exception
	 */
	@JsonView(Profile.FriendsView.class)
	@RequestMapping(value = "/characters-marvel", method = RequestMethod.GET)
	public MarvelCharactersModel getCharactersMarvel() throws Exception {		
		MarvelCharactersModel charactersModelList = new MarvelCharactersModel();
		charactersModelList.setCharactersList(marvelSingleton.getCharactersResults());		
		return charactersModelList;
	}
	
		
	/**
	 * Delete characters.
	 *
	 * @return the response entity
	 * @throws Exception the exception
	 */
	@RequestMapping(value = "/delete-characters", method = RequestMethod.GET)
	public ResponseEntity<String> deleteCharacters() throws Exception {	
		marvelSingleton.getCharactersResults().clear();		
		return new ResponseEntity<String>("Characters clear", HttpStatus.OK);
	}	
	
}
