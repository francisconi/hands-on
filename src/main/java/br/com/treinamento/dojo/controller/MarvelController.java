package br.com.treinamento.dojo.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.treinamento.dojo.characters.model.Characters;
import br.com.treinamento.dojo.comics.model.Comics;
import br.com.treinamento.dojo.exception.MarvelException;
import br.com.treinamento.dojo.service.CharactersService;
import br.com.treinamento.dojo.service.ComicsService;
import br.com.treinamento.dojo.singleton.MarvelSingleton;

// TODO: Auto-generated Javadoc
/**
 * The Class MarvelController.
 */
@RestController
public class MarvelController {

	/** The character service. */
	@Autowired
	private CharactersService characterService;

	/** The comics service. */
	@Autowired
	private ComicsService comicsService;
	

	/**
	 * Redirect to external url.
	 *
	 * @return the response entity
	 * @throws URISyntaxException the URI syntax exception
	 */
	@RequestMapping(value = "/helloworld", method = RequestMethod.GET)
	public ResponseEntity<Object> redirectToExternalUrl() throws URISyntaxException {
		URI yahoo = new URI("http://www.ci&t.com");
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setLocation(yahoo);
		return new ResponseEntity<>(httpHeaders, HttpStatus.SEE_OTHER);
	}

	/**
	 * Buscar character por nome.
	 *
	 * @param nome the nome
	 * @return the response entity
	 */
	@RequestMapping(value = "/character", method = RequestMethod.POST, produces = { "text/plain; charset=UTF-8" })
	public ResponseEntity<String> buscarCharacterPorNome(@RequestParam(value = "nome") String nome) {
		try {			
			String marvelCharacter = characterService.buscarInformacoesCharacter(nome);
			URI urlImage = new URI(returnImageUrl(marvelCharacter));			
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setLocation(urlImage);
			return new ResponseEntity<>(httpHeaders, HttpStatus.SEE_OTHER);

		} catch (Exception e) {
			return new ResponseEntity<String>("Erro ao carragar os dados", HttpStatus.BAD_REQUEST);
		}
	}

	/**
	 * Buscar comics por ID.
	 *
	 * @param idcomics the idcomics
	 * @return the response entity
	 */
	@RequestMapping(value = "/comics", method = RequestMethod.POST, produces = { "text/plain; charset=UTF-8" })
	public ResponseEntity<String> buscarComicsPorID(@RequestParam(value = "idcomics") String idcomics) {
		try {
			String marvelComics = comicsService.buscarComicsPorID(idcomics);
			URI urlComic = new URI(returnImageUrl(marvelComics));			
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setLocation(urlComic);
			return new ResponseEntity<>(httpHeaders, HttpStatus.SEE_OTHER);

		} catch (Exception e) {
			return new ResponseEntity<String>("Erro ao carragar os dados", HttpStatus.BAD_REQUEST);
		}
	}

	/**
	 * Return image url.
	 *
	 * @param total the total
	 * @return the string
	 */
	private String returnImageUrl(String total) {
		int inicio = total.lastIndexOf("\"thumbnail\":{\"path\":\"");
		int fim = total.indexOf("\",\"extension\":\"");
		String ss = total.substring(inicio, fim).substring(21) + "/portrait_xlarge.jpg";
		return ss;
	}

}
