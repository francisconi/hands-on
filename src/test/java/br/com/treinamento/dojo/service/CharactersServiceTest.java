package br.com.treinamento.dojo.service;


import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import br.com.treinamento.config.AppConfig;
import br.com.treinamento.dojo.exception.MarvelException;
import br.com.treinamento.dojo.singleton.MarvelSingleton;

// TODO: Auto-generated Javadoc
/**
 * The Class CharactersServiceTest.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppConfig.class, webEnvironment = WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class CharactersServiceTest {
	
	/** The character service. */
	@Autowired
	private CharactersService characterService;

	/** The marvel singleton. */
	@Autowired
	private MarvelSingleton marvelSingleton;

	/**
	 * Gerar exception.
	 *
	 * @throws Exception the exception
	 */
	@Test(expected = MarvelException.class)
	public void gerarException() throws Exception {
		characterService.buscarInformacoesCharacter(null);
	}
	

    /**
     * Verificar dados singleton.
     *
     * @throws Exception the exception
     */
    @Test
	public void verificarDadosSingleton() throws Exception {
		characterService.buscarInformacoesCharacter("Wolverine");		
		Assert.assertTrue(marvelSingleton.getCharactersResults().size()>0);
	}
    
    
}
